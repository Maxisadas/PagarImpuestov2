/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.*;
import Entidades.*;
import Persistencia.FachadaPersistencia;
import java.util.*;


public class ExpertoABMTipoEmpresa {
    TipoEmpresa tipoEmpresa;
    public void altaTipoEmpresa(DTOTipoEmpresaAlta dto){
        TipoEmpresa tipoEmpresa = new TipoEmpresa();
        tipoEmpresa.setCodigoTipoEmpresa(dto.getCodigo());
        tipoEmpresa.setNombreTipoEmpresa(dto.getNombre());
        Date fechaActual = new Date();
        tipoEmpresa.setFechaHabTipoEmpresa(fechaActual);
        tipoEmpresa.setFechaInhabTipoEmpresa(null);        
        FachadaPersistencia.getInstancia().guardar(tipoEmpresa);
    }

    public List<DTOTipoEmpresa> opcionAlta() {
        List<DTOCriterio> listaDTOCriterio = new ArrayList<>();
        DTOCriterio dtoCriterio = new DTOCriterio();
        dtoCriterio.setAtributo("fechaInhabTipoEmpresa");
        dtoCriterio.setOperador("is null");
        dtoCriterio.setValor(null);
        listaDTOCriterio.add(dtoCriterio);
        List listaTipoEmpresa = FachadaPersistencia.getInstancia().buscar("TipoEmpresa", listaDTOCriterio);
        List<DTOTipoEmpresa> listaDTOTipoEmpresa = new ArrayList<>();
        for(int i=0; i<listaTipoEmpresa.size();i++){
            TipoEmpresa tipoEmpresa = (TipoEmpresa)listaTipoEmpresa.get(i);
            DTOTipoEmpresa dtoTipoEmpresa = new DTOTipoEmpresa();
            dtoTipoEmpresa.setCodigo(tipoEmpresa.getCodigoTipoEmpresa());
            dtoTipoEmpresa.setNombre(tipoEmpresa.getNombreTipoEmpresa());
            listaDTOTipoEmpresa.add(dtoTipoEmpresa);
        }
        return listaDTOTipoEmpresa;
    }
    
    public List<DTOTipoEmpresaModificarTipoEmpresa> opcionModificar(){
        try{
            List listaTipoEmpresa = FachadaPersistencia.getInstancia().buscar("TipoEmpresa", null);
            List<DTOTipoEmpresaModificarTipoEmpresa> listaDTOTipoEmpresa = new ArrayList<>();
            for(int i=0; i<listaTipoEmpresa.size(); i++){
                TipoEmpresa tipoEmpresa = (TipoEmpresa)listaTipoEmpresa.get(i);
                DTOTipoEmpresaModificarTipoEmpresa dtoTipoEmpresa = new DTOTipoEmpresaModificarTipoEmpresa();
                dtoTipoEmpresa.setCodigo(tipoEmpresa.getCodigoTipoEmpresa());
                dtoTipoEmpresa.setNombre(tipoEmpresa.getNombreTipoEmpresa());
                dtoTipoEmpresa.setFechaHab(tipoEmpresa.getFechaHabTipoEmpresa());
                dtoTipoEmpresa.setFechaInhab(tipoEmpresa.getFechaInhabTipoEmpresa());
                listaDTOTipoEmpresa.add(dtoTipoEmpresa);
            }
            return listaDTOTipoEmpresa;
        }catch(NullPointerException e){}
        return null;
    }
   
    public void ingresarModificacionTipoEmpresa(DTOTipoEmpresaModificar dtoTDM){
        int codigoTipoEmpresa = dtoTDM.getCodigo();
        List<DTOCriterio> listaDTOCriterio = new ArrayList<>();
        DTOCriterio dtoCriterio = new DTOCriterio();
        dtoCriterio.setAtributo("codTipoEmpresa");
        dtoCriterio.setOperador("=");
        dtoCriterio.setValor(codigoTipoEmpresa);
        listaDTOCriterio.add(dtoCriterio);
        TipoEmpresa tipoEmpresaLocal = (TipoEmpresa)(FachadaPersistencia.getInstancia().buscar("TipoEmpresa", listaDTOCriterio)).get(0);
        tipoEmpresaLocal.setNombreTipoEmpresa(dtoTDM.getNombre());
        FachadaPersistencia.getInstancia().guardar(tipoEmpresaLocal);
    }
    
     @SuppressWarnings("empty-statement")
    public void ingresarAlta (DTOTipoEmpresaAlta dtoTipoEmpresaAlta){
        tipoEmpresa = new TipoEmpresa();
        Date fechaActual = new Date();
        tipoEmpresa.setNombreTipoEmpresa(dtoTipoEmpresaAlta.getNombre());
        tipoEmpresa.setCodigoTipoEmpresa(dtoTipoEmpresaAlta.getCodigo());
        tipoEmpresa.setFechaHabTipoEmpresa(fechaActual);        
        FachadaPersistencia.getInstancia().guardar(tipoEmpresa);
    }
    
      public boolean baja(DTOTipoEmpresaBaja dtoTipoEmpresaBaja){
        List<DTOCriterio> listaDTOCriterio = new ArrayList<>();
        DTOCriterio dtoCriterio = new DTOCriterio();
        dtoCriterio.setAtributo("codTipoEmpresa");
        dtoCriterio.setOperador("=");
        dtoCriterio.setValor(dtoTipoEmpresaBaja.getCodigo());
        listaDTOCriterio.add(dtoCriterio);
        tipoEmpresa = (TipoEmpresa)(FachadaPersistencia.getInstancia().buscar("TipoEmpresa", listaDTOCriterio)).get(0);
        Date fechaActual = new Date();
        tipoEmpresa.setFechaInhabTipoEmpresa(fechaActual);
        try{
            FachadaPersistencia.getInstancia().guardar(tipoEmpresa);
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
}

