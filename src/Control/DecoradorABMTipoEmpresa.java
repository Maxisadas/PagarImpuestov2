/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.*;
import Persistencia.FachadaInterna;
import java.util.List;

public class DecoradorABMTipoEmpresa extends ExpertoABMTipoEmpresa{
     public void iniciarT(){        
        FachadaInterna.getInstancia().iniciarTransaccion();
    }
      @Override
      public void altaTipoEmpresa(DTOTipoEmpresaAlta dto){
        super.altaTipoEmpresa(dto);
        FachadaInterna.getInstancia().finalizarTransaccion();
    }
    @Override
      public List<DTOTipoEmpresa> opcionAlta() {
        return super.opcionAlta();
    }
    @Override
    public void ingresarModificacionTipoEmpresa(DTOTipoEmpresaModificar dtoAM){
        super.ingresarModificacionTipoEmpresa(dtoAM);
        FachadaInterna.getInstancia().finalizarTransaccion();
    }
    
     public void ingresarAlta(DTOTipoEmpresaAlta dtoTipoEmpresaAlta){
        super.ingresarAlta(dtoTipoEmpresaAlta);
        FachadaInterna.getInstancia().finalizarTransaccion();
    }
      @Override
    public boolean baja(DTOTipoEmpresaBaja dtoTipoEmpresaBaja){
        try{
            super.baja(dtoTipoEmpresaBaja);
            FachadaInterna.getInstancia().finalizarTransaccion();
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
}

