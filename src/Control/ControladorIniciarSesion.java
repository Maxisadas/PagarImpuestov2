/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

/**
 *
 * @author tinch
 */
public class ControladorIniciarSesion {
    private DecoradorIniciarSesion decorador;
    
    public boolean iniciarSesion(String user, String password) {
        return decorador.iniciarSesion(user,password);
    }

    public void iniciarT() {
      this.decorador = (DecoradorIniciarSesion) FabricaExpertos.getInstancia().crearExperto("Iniciar Sesión"); 
      decorador.iniciarT();
    }
}
