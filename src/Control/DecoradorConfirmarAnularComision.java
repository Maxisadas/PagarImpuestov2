/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.DTOEmpresa;
import Persistencia.FachadaInterna;
import java.util.List;

public class DecoradorConfirmarAnularComision extends ExpertoConfirmarAnularComision {

    
    @Override
    public List<DTOEmpresa> buscarEmpresas() {
        FachadaInterna.getInstancia().iniciarTransaccion();
        return super.buscarEmpresas();
    }
    
    
}
