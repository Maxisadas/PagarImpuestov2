/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Entidades.*;
import DTO.*;
import Persistencia.FachadaPersistencia;
import java.util.*;

/**
 *
 * @author phantom2024
 */
public class ExpertoPagarImpuesto {

    private Operacion operacion;

    public List<DTOTipoImpuesto> pagarImpuesto() {

        List<DTOCriterio> listaDTOCriterio = new ArrayList<>();
        DTOCriterio dtoCriterio = new DTOCriterio();
        dtoCriterio.setAtributo("nombreUsuario");
        dtoCriterio.setOperador("=");
        dtoCriterio.setValor("Fernando");
        listaDTOCriterio.add(dtoCriterio);

        List<Object> listusuario = FachadaPersistencia.getInstancia().buscar("Usuario", listaDTOCriterio);

        Usuario usuario = (Usuario) listusuario.get(0);

        List<DTOTipoImpuesto> lista_dto_ti = new ArrayList<>();

        Rol rol = usuario.getRol();
        List<RolOpcion> lista_rol_opcion = rol.getListRolOpcion();

        for (int i = 0; i < lista_rol_opcion.size(); i++) {
            if (lista_rol_opcion.get(i).getNombreRolOpcion().equals("CUPAGARIMPUESTO")) {

                Cliente cliente = usuario.getCliente();

                operacion = new Operacion();

                operacion.setCliente(cliente);

                DTOCriterio c2 = new DTOCriterio("fechaInhabilitacionTipoImpuesto", "is null", null);

                List<DTOCriterio> l2 = new ArrayList<>();
                l2.add(c2);

                List<Object> list_tipo_impuesto = FachadaPersistencia.getInstancia().buscar("TipoImpuesto", l2);

                for (int u = 0; u < list_tipo_impuesto.size(); u++) {

                    TipoImpuesto tipoimpuesto = (TipoImpuesto) list_tipo_impuesto.get(u);

                    DTOTipoImpuesto dto_tipo_impuesto = new DTOTipoImpuesto();
                    dto_tipo_impuesto.setCodigo(tipoimpuesto.getcodigoTipoImpuesto());
                    dto_tipo_impuesto.setNombre(tipoimpuesto.getNombreTipoImpuesto());

                    lista_dto_ti.add(dto_tipo_impuesto);

                }

            }
        }

        return lista_dto_ti;

    }

    public List<DTOEmpresa> seleccionarTipoImpuesto(int codigoTipoImpuesto) {

        List<DTOCriterio> l5 = new ArrayList<>();
        DTOCriterio c6 = new DTOCriterio();
        c6.setAtributo("codigoTipoImpuesto");
        c6.setOperador("=");
        c6.setValor(codigoTipoImpuesto);
        l5.add(c6);

        List<Object> list_tipo_impuesto = FachadaPersistencia.getInstancia().buscar("TipoImpuesto", l5);
        TipoImpuesto tipo_impuesto = (TipoImpuesto) list_tipo_impuesto.get(0);

        List<DTOCriterio> l3 = new ArrayList<>();

        DTOCriterio c3 = new DTOCriterio();
        c3.setAtributo("fechaInhabilitacionEmpresaTipoImpuesto");
        c3.setOperador("is null");
        c3.setValor(null);
        l3.add(c3);

        DTOCriterio c4 = new DTOCriterio();
        c4.setAtributo("tipoImpuesto");
        c4.setOperador("=");
        c4.setValor(tipo_impuesto);
        l3.add(c4);

        List<Object> list_empresa_tipo_impuesto = FachadaPersistencia.getInstancia().buscar("EmpresaTipoImpuesto", l3);

        List<DTOEmpresa> lista_dto_empresa = new ArrayList<>();

        for (int i = 0; i < list_empresa_tipo_impuesto.size(); i++) {

            EmpresaTipoImpuesto empresa_tipo_impuesto = (EmpresaTipoImpuesto) list_empresa_tipo_impuesto.get(i);
            DTOEmpresa dto_empresa = new DTOEmpresa();

            dto_empresa.setCodigoEmpresaTipoImpuesto(empresa_tipo_impuesto.getCodigoEmpresaTipoImpuesto());
            dto_empresa.setNombreEmpresa(empresa_tipo_impuesto.getEmpresa().getNombreEmpresa());

            lista_dto_empresa.add(dto_empresa);

        }

        return lista_dto_empresa;
    }

    public void seleccionarEmpresa(int codigoEmpresaTipoImpuesto) {

        List<DTOCriterio> l4 = new ArrayList<>();
        DTOCriterio c5 = new DTOCriterio();
        c5.setAtributo("codigoEmpresaTipoImpuesto");
        c5.setOperador("=");
        c5.setValor(codigoEmpresaTipoImpuesto);
        l4.add(c5);

        List<Object> list_empresa_tipo_impuesto = FachadaPersistencia.getInstancia().buscar("EmpresaTipoImpuesto", l4);
        EmpresaTipoImpuesto empresa_tipo_impuesto = (EmpresaTipoImpuesto) list_empresa_tipo_impuesto.get(0);

        operacion.setEmpresaTipoImpuesto(empresa_tipo_impuesto);
        

    }

    public List<DTOComprobanteImpago> ingresarCodPagoElectronico(String codigoPagoElectronico){
        
        operacion.setCodPagoElect(codigoPagoElectronico);
            
            EmpresaTipoImpuesto empresa_tipo_impuesto = operacion.getEmpresaTipoImpuesto();
            Empresa empresa = empresa_tipo_impuesto.getEmpresa();
            
            AdaptadorEmpresa adaptador_empresa =  FabricaAdaptadorEmpresa.getInstancia().obtenerAdaptadorEmpresa(empresa);
            
            if (adaptador_empresa instanceof AdaptadorClaro) {
                AdaptadorClaro adaptador_claro = (AdaptadorClaro) adaptador_empresa;
              List<DTOComprobanteImpago> list_dto_comprobante_impago = adaptador_claro.obtenerComprobantesImpagos(codigoPagoElectronico,empresa_tipo_impuesto);
              return list_dto_comprobante_impago;
                
            }
            if (adaptador_empresa instanceof AdaptadorMovistar) {
                return null;
            }
        
         return null;       
        
    }
}
