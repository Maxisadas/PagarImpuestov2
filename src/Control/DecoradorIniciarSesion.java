/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Persistencia.FachadaInterna;

/**
 *
 * @author tinch
 */
public class DecoradorIniciarSesion extends ExpertoIniciarSesion{

    /**
     *
     * @param user
     * @param password
     * @return
     */
    @Override
    public boolean iniciarSesion(String user, String password) {
        return super.iniciarSesion(user,password);
    }

    void iniciarT() {
        FachadaInterna.getInstancia().iniciarTransaccion();
    }
    
}
