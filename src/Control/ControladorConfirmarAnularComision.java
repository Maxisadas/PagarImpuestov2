/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.*;
import java.util.List;

public class ControladorConfirmarAnularComision {
   
    private ExpertoConfirmarAnularComision experto;

    public ControladorConfirmarAnularComision() {
        
       this.experto = (ExpertoConfirmarAnularComision) FabricaExpertos.getInstancia().crearExperto("ConfirmarAnularComision"); 
        
        
    }
  
    public List<DTOEmpresa> buscarEmpresas() {
       return experto.buscarEmpresas();
    }

    public List<DTOComisionEmpresaSistema> buscarComisionEmpresaSistema(DTOEmpresa empresa) {
       return experto.buscarComisionEmpresaSistema(empresa);
    }  
      
}

