/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.DTOComprobanteImpago;
import Entidades.EmpresaTipoImpuesto;
import Entidades.Operacion;
import java.util.List;

/**
 *
 * @author phantom2024
 */
public interface AdaptadorEmpresa {
    
    List<DTOComprobanteImpago> obtenerComprobantesImpagos(String codigoPagoElectronico, EmpresaTipoImpuesto empresa_tipo_impuesto);
    
}
