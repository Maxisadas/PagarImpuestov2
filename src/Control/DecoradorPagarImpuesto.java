/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.DTOComprobanteImpago;
import DTO.DTOEmpresa;
import DTO.DTOTipoImpuesto;
import Persistencia.FachadaInterna;
import java.util.List;

/**
 *
 * @author phantom2024
 */
public class DecoradorPagarImpuesto extends ExpertoPagarImpuesto {
    
    public void iniciarTransaccion(){
        FachadaInterna.getInstancia().iniciarTransaccion();
    }

    public List<DTOTipoImpuesto> pagarImpuesto(String nombreUsuario) {

        return super.pagarImpuesto();
    }

    public List<DTOEmpresa> seleccionarTipoImpuesto(int codigoTipoImpuesto) {

        return super.seleccionarTipoImpuesto(codigoTipoImpuesto);
    }

    public void seleccionarEmpresa(int codigoEmpresaTipoImpuesto) {

        super.seleccionarEmpresa(codigoEmpresaTipoImpuesto);

    }

    public List<DTOComprobanteImpago> ingresarCodPagoElectronico(String codigoPagoElectronico) {

        return super.ingresarCodPagoElectronico(codigoPagoElectronico);
    }
}
