/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.DTOComprobanteImpago;
import DTO.DTOEmpresa;
import DTO.DTOTipoImpuesto;
import java.util.List;

/**
 *
 * @author phantom2024
 */
public class ControladorPagarImpuesto {

    private DecoradorPagarImpuesto decorador_pagar_impuesto;
    
    public ControladorPagarImpuesto(){
        FabricaExpertos fabrica_expertos = FabricaExpertos.getInstancia();
        decorador_pagar_impuesto = (DecoradorPagarImpuesto) fabrica_expertos.crearExperto("PagarImpuesto");
        decorador_pagar_impuesto.iniciarTransaccion();
    }

    public List<DTOTipoImpuesto> pagarImpuesto() {

        List<DTOTipoImpuesto> lista_tipo_impuesto = decorador_pagar_impuesto.pagarImpuesto("Fernando");
        return lista_tipo_impuesto;
    }

    public List<DTOEmpresa> seleccionarTipoImpuesto(int codigoTipoImpuesto) {

        return decorador_pagar_impuesto.seleccionarTipoImpuesto(codigoTipoImpuesto);
    }

    public void seleccionarEmpresa(int codigoEmpresaTipoImpuesto) {

        decorador_pagar_impuesto.seleccionarTipoImpuesto(codigoEmpresaTipoImpuesto);
    }
    
    public List<DTOComprobanteImpago> ingresarCodPagoElectronico(String codigoPagoElectronico){
        
        return decorador_pagar_impuesto.ingresarCodPagoElectronico(codigoPagoElectronico);
    }
}
