/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.*;
import java.util.List;

public class ControladorABMTipoEmpresa {
   private DecoradorABMTipoEmpresa decorador;
   
     public void iniciarT(){
      this.decorador = (DecoradorABMTipoEmpresa) FabricaExpertos.getInstancia().crearExperto("ABM TipoEmpresa"); 
      decorador.iniciarT();
    }
   
    public void altaTipoEmpresa(DTOTipoEmpresaAlta dto){
        decorador.altaTipoEmpresa(dto);
    }
    
    public List<DTOTipoEmpresaModificarTipoEmpresa> opcionModificar(){
        return decorador.opcionModificar();
    }
   
    public List<DTOTipoEmpresa> opcionAlta() {
        return decorador.opcionAlta();
    }
    
    public void ingresarAlta (DTOTipoEmpresaAlta dtoTipoEmpresaAlta){
        decorador.ingresarAlta(dtoTipoEmpresaAlta);
    }
    public void ingresarModificacionTipoEmpresa(DTOTipoEmpresaModificar dtoMTP){
        decorador.ingresarModificacionTipoEmpresa(dtoMTP);
    }
    
    public boolean baja(DTOTipoEmpresaBaja dtoTipoEmpresaBaja){
        return decorador.baja(dtoTipoEmpresaBaja);
    }
}

