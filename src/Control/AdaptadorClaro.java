/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.*;
import Entidades.Atributo;
import Entidades.EmpresaTipoImpuesto;
import Entidades.EmpresaTipoImpuestoAtributo;
import Entidades.Operacion;
import Entidades.TipoDato;
import Entidades.TipoImpuestoAtributo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author phantom2024
 */
public class AdaptadorClaro implements AdaptadorEmpresa{

    @Override
    public List<DTOComprobanteImpago> obtenerComprobantesImpagos(String codigoPagoElectronico, EmpresaTipoImpuesto empresa_tipo_impuesto) {
       //ACA VA LA CONEXION CON AL EMPRESA 
        DTOClaro dto_claro = new DTOClaro();
        List<DTOComprobanteImpago> list_dto_comprobante_impago = new ArrayList<>();
        List<DTOComprobanteImpagoAtributo> list_dto_cia = new ArrayList<>();
        
            //ACA VA EL BUCLE DE POR CADA COMPROBANTE ENCONTRADO.

            DTOComprobanteImpago dto_comprobante_impago = new DTOComprobanteImpago();
            int numeroComprobante = Integer.parseInt(dto_claro.getNumeroComprobante());
            
            dto_comprobante_impago.setNumeroComprobanteImpago(numeroComprobante);
            
           
            List<EmpresaTipoImpuestoAtributo> empresaTIA = empresa_tipo_impuesto.getEmpresaTIA();
            
         
            for (int i = 0; i < empresaTIA.size(); i++) {
                DTOComprobanteImpagoAtributo dto_comprobante_impago_atributo = new DTOComprobanteImpagoAtributo();
                EmpresaTipoImpuestoAtributo empresa_tia = empresaTIA.get(i);
                TipoImpuestoAtributo tipoimpuestoatributo = empresa_tia.getTipoimpuestoatributo();
                Atributo atributo =tipoimpuestoatributo.getAtributo();
         
                
                if(atributo.getNombreAtributo() == "Importe"){
             dto_comprobante_impago_atributo.setTipoImpuestoAtributo(tipoimpuestoatributo);
             dto_comprobante_impago_atributo.setValor(dto_claro.getImporte());
                    
                }
                if(atributo.getNombreAtributo() == "Fecha de vencimiento"){
             dto_comprobante_impago_atributo.setTipoImpuestoAtributo(tipoimpuestoatributo);
             dto_comprobante_impago_atributo.setValor(dto_claro.getFacturaVencimiento());
                    
                }
                if(atributo.getNombreAtributo() == "Fecha proximo vencimiento"){
             dto_comprobante_impago_atributo.setTipoImpuestoAtributo(tipoimpuestoatributo);
             dto_comprobante_impago_atributo.setValor(dto_claro.getProximoVencimiento());
                    
                }
                if(atributo.getNombreAtributo() == "Monto minimo exigible"){
             dto_comprobante_impago_atributo.setTipoImpuestoAtributo(tipoimpuestoatributo);
             dto_comprobante_impago_atributo.setValor(dto_claro.getMinimoMontoExigible());
                    
                }
                
                list_dto_cia.add(dto_comprobante_impago_atributo);
                
            }
            
            dto_comprobante_impago.setList_dto_cia(list_dto_cia);
            list_dto_comprobante_impago.add(dto_comprobante_impago);
        

        return list_dto_comprobante_impago;
        
    }

}
