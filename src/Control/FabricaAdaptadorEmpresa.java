/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Entidades.Empresa;

/**
 *
 * @author phantom2024
 */
public class FabricaAdaptadorEmpresa {
    
    private static FabricaAdaptadorEmpresa instancia;
    
    public static FabricaAdaptadorEmpresa getInstancia (){
        if (instancia == null){
            instancia = new FabricaAdaptadorEmpresa();
        }
        return instancia;
    }
    
    
    public AdaptadorEmpresa obtenerAdaptadorEmpresa(Empresa empresa){
        
        switch (empresa.getNombreEmpresa())
        {
            case "Movistar":
                return new AdaptadorMovistar();
            case "Claro":
                return new AdaptadorClaro();
            default:
                return null;
        }
        
        
    }
}
