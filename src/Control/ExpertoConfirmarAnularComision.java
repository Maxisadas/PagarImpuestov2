/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.*;
import Entidades.*;
import Persistencia.FachadaPersistencia;
import java.util.*;

public class ExpertoConfirmarAnularComision {

   
    public List<DTOEmpresa> buscarEmpresas() {

        List listaEmpresa = FachadaPersistencia.getInstancia().buscar("Empresa", null);
        List<DTOEmpresa> empresas = new ArrayList<>();

        for (Object emp : listaEmpresa) {
            //LO combierto a emrpesa
            Empresa e = (Empresa) emp;

            //Creo el dto y lo lleno
            DTOEmpresa dtoe = new DTOEmpresa();
            dtoe.setCuitEmpresa(e.getCuitEmpresa());
            dtoe.setNombreEmpresa(e.getNombreEmpresa());

            //Agrego el dto a la lista a devolver
            empresas.add(dtoe);

        }

        return empresas;
    }

    List<DTOComisionEmpresaSistema> buscarComisionEmpresaSistema(DTOEmpresa empresadto) {

        String cuitEmpresa = empresadto.getCuitEmpresa();

        //Busco la empresa ingresada
        List<DTOCriterio> listaEmpresaIngresada = new ArrayList<>();
        DTOCriterio dtoBusqEmpresa = new DTOCriterio();
        dtoBusqEmpresa.setAtributo("cuitEmpresa");
        dtoBusqEmpresa.setOperador("=");
        dtoBusqEmpresa.setValor(cuitEmpresa);
        listaEmpresaIngresada.add(dtoBusqEmpresa);

        Empresa empresa = (Empresa) (FachadaPersistencia.getInstancia().buscar("Empresa", listaEmpresaIngresada)).get(0);

        //Luego Busco las comisiones para la empresa ingresada
        List<DTOCriterio> listaEmpresaTI = new ArrayList<>();
        DTOCriterio dtoBusqEMpresaTI = new DTOCriterio();
        dtoBusqEMpresaTI.setAtributo("empresa");
        dtoBusqEMpresaTI.setOperador("=");
        dtoBusqEMpresaTI.setValor(empresa);
        listaEmpresaTI.add(dtoBusqEMpresaTI);

        List empresaTipoImpuestoList = (FachadaPersistencia.getInstancia().buscar("EmpresaTipoImpuesto", listaEmpresaTI));

        //Para cada empresa tipo de impuesto consigo sus comisiones con estado pendiente
        
        List<ComisionEmpresaSistema> comisiones = new ArrayList<>();

        
        for (Object object : empresaTipoImpuestoList) {

            List<DTOCriterio> listaComisiones = new ArrayList<>();
            EmpresaTipoImpuesto eti = (EmpresaTipoImpuesto) object;

            DTOCriterio dtoBusqComisionETI = new DTOCriterio();
            dtoBusqComisionETI.setAtributo("empresaTipoImpuesto");
            dtoBusqComisionETI.setOperador("=");
            dtoBusqComisionETI.setValor(eti);

            listaComisiones.add(dtoBusqComisionETI);
            
            List comisionesETIList = (FachadaPersistencia.getInstancia().buscar("ComisionEmpresaSistema", listaComisiones));
             
            
            for (Object object1 : comisionesETIList) {
                empresaTipoImpuestoList.add((ComisionEmpresaSistema)object1);
            }
            

        }
        
        
        //Una vez que tengo todas las comisiones para cada empresatipoimpuesto de esa empresa, 
        //Las filtro por las que estan en estado pensiente
   
 
        //Creo una lista de los dtos a devolver
        List<DTOComisionEmpresaSistema> dtocomisionList = new ArrayList<>();
        
        for (ComisionEmpresaSistema comision : comisiones ) {
          
     
           //Primero los ordeno a todos sus estado en el tiempo por fecha
           comision.getEstadoComisionEmpresaSistema().sort(
                    Comparator.comparing(a -> a.getFechaEstadoCES()));
           
           
           //Obtengo el ultimo que esta en la posicion tamaño menos uno 
           EstadoComisionEmpresaSistema estadoAtual_i = 
                 comision.getEstadoComisionEmpresaSistema().get(
                         comision.getEstadoComisionEmpresaSistema().size()-1);
            
           
            String nombreEstado = estadoAtual_i.getComisionEmpresaSistemaEstado().getNombreEstadoCES();
            
            if (nombreEstado.equals("Pendiente") || nombreEstado.equals("Recalculado")) {
                
                DTOComisionEmpresaSistema tmp = new DTOComisionEmpresaSistema();
                tmp.setEstado(nombreEstado);
                tmp.setCodigoCES(comision.getCodigoCES());
                tmp.setValorPeriodo(comision.getPeriodicidadUtilizada()+"");
                tmp.setNombreTipoImpuesto(comision.getEmpresaTipoImpuesto().getTipoImpuesto().getNombreTipoImpuesto());
                tmp.setPorcentajeUtilizado(comision.getPorcentajeUtilizado());
                tmp.setFechaComision(comision.getFechaComisionEmpresaSistema());
                
                tmp.setCantidadOperaciones(comision.getComisiones().size());
                //Con esta magia calculo el total 
                tmp.setTotalComision(
                        comision.getComisiones().stream().mapToDouble(x -> x.getValorComision()).sum());
                
                
                dtocomisionList.add(tmp);
            }
           
        }
        
        //A este punto tengo la lista llena para devolver
        return dtocomisionList;

    }

}
