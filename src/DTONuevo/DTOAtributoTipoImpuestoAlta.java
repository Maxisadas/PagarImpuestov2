/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTONuevo;


/**
 *
 * @author Lorenzo
 */
public class DTOAtributoTipoImpuestoAlta {
    private int orden;
    private String nombre;
    public DTOAtributoTipoImpuestoAlta(){
        
    }
    public DTOAtributoTipoImpuestoAlta(int orden, String nombre) {
        this.orden = orden;
        this.nombre = nombre;
    }

    public int getOrden() {
        return orden;
    }

    public String getNombre() {
        return nombre;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
