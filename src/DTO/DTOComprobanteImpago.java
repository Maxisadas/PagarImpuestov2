/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.List;

/**
 *
 * @author phantom2024
 */
public class DTOComprobanteImpago {

    private int numeroComprobanteImpago;
    private List<DTOComprobanteImpagoAtributo> list_dto_cia;

    public List<DTOComprobanteImpagoAtributo> getList_dto_cia() {
        return list_dto_cia;
    }

    public void setList_dto_cia(List<DTOComprobanteImpagoAtributo> list_dto_cia) {
        this.list_dto_cia = list_dto_cia;
    } 
    
    public DTOComprobanteImpago() {
    }
    
    public int getNumeroComprobanteImpago() {
        return numeroComprobanteImpago;
    }

    public void setNumeroComprobanteImpago(int numeroComprobanteImpago) {
        this.numeroComprobanteImpago = numeroComprobanteImpago;
    }
    
}
