/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;
import java.util.Date;

public class DTOTipoDatoModificarTipoDato {
     private String nombre;
    private int codigo;
    private Date fechaHab;
    private Date fechaInhab;
    
    public DTOTipoDatoModificarTipoDato(){
        
    }
    public DTOTipoDatoModificarTipoDato(int codigo, String nombre, Date fechaHab, Date fechaInhab){
        this.codigo = codigo;
        this.nombre = nombre;
        this.fechaHab = fechaHab;
        this.fechaInhab = fechaInhab;
    }
    
     public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    public Date getFechaHab() {
        return fechaHab;
    }

    public void setFechaHab(Date fechaHab) {
        this.fechaHab = fechaHab;
    }

    public Date getFechaInhab() {
        return fechaInhab;
    }

    public void setFechaInhab(Date fechaInhab) {
        this.fechaInhab = fechaInhab;
    }
}
