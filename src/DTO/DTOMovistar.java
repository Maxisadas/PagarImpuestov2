/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;

/**
 *
 * @author phantom2024
 */
public class DTOMovistar {
    
    private double minimoMontoExigible = 100;
    private Date facturaVencimiento = new Date(2017, 9, 19);
    private double importe = 1000;
    private int numeroComprobante = 163462;
    private Date proximoVencimiento = new Date(2017, 10, 19);

    public DTOMovistar() {
    }

    public double getMinimoMontoExigible() {
        return minimoMontoExigible;
    }

    public void setMinimoMontoExigible(double minimoMontoExigible) {
        this.minimoMontoExigible = minimoMontoExigible;
    }

    public Date getFacturaVencimiento() {
        return facturaVencimiento;
    }

    public void setFacturaVencimiento(Date facturaVencimiento) {
        this.facturaVencimiento = facturaVencimiento;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public int getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(int numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }

    public Date getProximoVencimiento() {
        return proximoVencimiento;
    }

    public void setProximoVencimiento(Date proximoVencimiento) {
        this.proximoVencimiento = proximoVencimiento;
    }
}
