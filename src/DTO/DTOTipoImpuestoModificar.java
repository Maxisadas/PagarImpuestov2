/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Mercedes
 */
public class DTOTipoImpuestoModificar {
    private int codigoTI;
    private String NombreTI;
    private Date fechaHab;
    private Date fechaInhab;
    private boolean modificable;
    private ArrayList<DTOTipoImpuestoAtributo> listaDTOTipoImpuestoAtributo = new ArrayList<>();

    public DTOTipoImpuestoModificar() {
    }

    public DTOTipoImpuestoModificar(int codigoTI, String NombreTI, Date fechaHab, Date fechaInhab, boolean modificable) {
        this.codigoTI = codigoTI;
        this.NombreTI = NombreTI;
        this.fechaHab = fechaHab;
        this.fechaInhab = fechaInhab;
        this.modificable = modificable;
    }

    public Date getFechaHab() {
        return fechaHab;
    }

    public void setFechaHab(Date fechaHab) {
        this.fechaHab = fechaHab;
    }

    public Date getFechaInhab() {
        return fechaInhab;
    }

    public void setFechaInhab(Date fechaInhab) {
        this.fechaInhab = fechaInhab;
    }

    public boolean isModificable() {
        return modificable;
    }

    public void setModificable(boolean modificable) {
        this.modificable = modificable;
    }
    
    public int getCodigoTI() {
        return codigoTI;
    }

    public void setCodigoTI(int codigoTI) {
        this.codigoTI = codigoTI;
    }

    public String getNombreTI() {
        return NombreTI;
    }

    public void setNombreTI(String NombreTI) {
        this.NombreTI = NombreTI;
    }
     
    public ArrayList<DTOTipoImpuestoAtributo> getListaDTOTipoImpuestoAtributo() {
        return listaDTOTipoImpuestoAtributo;
    }

    public void setListaDTOTipoImpuestoAtributo(ArrayList<DTOTipoImpuestoAtributo> listaDTOTipoImpuestoAtributo) {
        this.listaDTOTipoImpuestoAtributo = listaDTOTipoImpuestoAtributo;
    }

   
}
