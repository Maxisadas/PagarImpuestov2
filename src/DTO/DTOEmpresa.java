/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;
import Entidades.*;
import java.util.Date;

public class DTOEmpresa {
    private String cuitEmpresa;
    private String nombreEmpresa;
    private Date fechaHabEmpresa;
    private Date fechaInhabEmpresa;
    private TipoEmpresa tipoEmpresa;
    private int codigoEmpresaTipoImpuesto;

    public int getCodigoEmpresaTipoImpuesto() {
        return codigoEmpresaTipoImpuesto;
    }

    public void setCodigoEmpresaTipoImpuesto(int codigoEmpresaTipoImpuesto) {
        this.codigoEmpresaTipoImpuesto = codigoEmpresaTipoImpuesto;
    }
    
    public DTOEmpresa(){
        
    }
    public DTOEmpresa(String cuit, String nombre, Date fechaHab, Date fechaInhab, TipoEmpresa tipo){
        this.cuitEmpresa = cuit;
        this.nombreEmpresa = nombre;
        this.fechaHabEmpresa = fechaHab;
        this.fechaInhabEmpresa = fechaInhab;
        this.tipoEmpresa = tipo;
       
    }
    
      public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombre) {
        this.nombreEmpresa = nombre;
    }

    public String getCuitEmpresa() {
        return cuitEmpresa;
    }

    public void setCuitEmpresa(String cuit) {
        this.cuitEmpresa = cuit;
    }

    public Date getFechaHabEmpresa() {
        return fechaHabEmpresa;
    }

    public void setFechaHabEmpresa(Date fechaHab) {
        this.fechaHabEmpresa = fechaHab;
    }
    
     public Date getFechaInhabEmpresa() {
        return fechaInhabEmpresa;
    }

    public void setFechaInhabEmpresa(Date fechaInhab) {
        this.fechaInhabEmpresa = fechaInhab;
    }
    
    public TipoEmpresa getTipoEmpresa(){
        return tipoEmpresa;
    }
    public void setTipoEmpresa(TipoEmpresa tipo){
        this.tipoEmpresa = tipo;
    }


    
    
    
}
