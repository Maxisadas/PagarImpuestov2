/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author tinch
 */
public class DTOAtributoAltaAtributo {
    private String nombre;
    private int longitud;
    private DTOTipoDato dtoTipoDato;

    public DTOAtributoAltaAtributo() {
    }

    public DTOAtributoAltaAtributo(String nombre, int longitud) {
        this.nombre = nombre;
        this.longitud = longitud;
    }

    public DTOAtributoAltaAtributo(String nombre, int longitud, DTOTipoDato dtoTipoDato) {
        this.nombre = nombre;
        this.longitud = longitud;
        this.dtoTipoDato = dtoTipoDato;
    }
    
    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public DTOTipoDato getDtoTipoDato() {
        return dtoTipoDato;
    }

    public void setDtoTipoDato(DTOTipoDato dtoTipoDato) {
        this.dtoTipoDato = dtoTipoDato;
    }
    
}
