/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.ArrayList;



/**
 *
 * @author Fernando
 */
public class DTOTipoImpuestoAlta {
    
    private String nombre;
    private boolean modificable;
    private ArrayList<DTOTipoImpuestoAtributoAlta> listaDTOTipoImpuestoAtributoAlta = new ArrayList();
    private int codigo; 

    
    
    public DTOTipoImpuestoAlta() {
    }

    public DTOTipoImpuestoAlta(String nombre, boolean modificable, int codigo) {
        this.nombre = nombre;
        this.modificable = modificable;
        this.codigo = codigo;
    }
     public DTOTipoImpuestoAlta(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isModificable() {
        return modificable;
    }

    public void setModificable(boolean modificable) {
        this.modificable = modificable;
    }

    public ArrayList<DTOTipoImpuestoAtributoAlta> getListaDTOTipoImpuestoAtributoAlta() {
        return listaDTOTipoImpuestoAtributoAlta;
    }

    public void addDTOTipoImpuestoAtributoAlta(DTOTipoImpuestoAtributoAlta DTO) {
        this.listaDTOTipoImpuestoAtributoAlta.add(DTO);
    }
    
    public void setListaDTOTipoImpuestoAtributoAlta(ArrayList<DTOTipoImpuestoAtributoAlta> DTOAtibutoOrden) {
        this.listaDTOTipoImpuestoAtributoAlta = DTOAtibutoOrden;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public int getCodigo() {
        return codigo;
    }
    
}
