/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

public class DTOTipoEmpresaModificar {
    private String nombreTipoEmpresa;
    private int codigoTipoEmpresa;
    
    public DTOTipoEmpresaModificar(){
    }
    
    public DTOTipoEmpresaModificar(int codigo, String nombre){
        this.codigoTipoEmpresa = codigo;
        this.nombreTipoEmpresa = nombre;
    }
    
    public int getCodigo() {
        return codigoTipoEmpresa;
    }

    public void setCodigo(int codigo) {
        this.codigoTipoEmpresa = codigo;
    }

    public String getNombre() {
        return nombreTipoEmpresa;
    }

    public void setNombre(String nombre) {
        this.nombreTipoEmpresa = nombre;
    }
}

