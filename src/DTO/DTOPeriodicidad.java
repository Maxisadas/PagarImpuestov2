/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Emanuel
 */
public class DTOPeriodicidad {
    private String nombrePeriodicidad;
    private int codigoPeriodicidad;
    private int diasPeriodicidad;
    
     public DTOPeriodicidad(String nombre, int codigo, int dias) {
        this.nombrePeriodicidad = nombre;
        this.codigoPeriodicidad = codigo;
        this.diasPeriodicidad = dias;
    }

    public DTOPeriodicidad() {
    }

    

    public int getCodigoPeriodicidad() {
        return codigoPeriodicidad;
    }

    public void setCodigoPeriodicidad(int codigo) {
        this.codigoPeriodicidad = codigo;
    }

    public String getNombrePeriodicidad() {
        return nombrePeriodicidad;
    }

    public void setNombrePeriodicidad(String nombre) {
        this.nombrePeriodicidad = nombre;
    }
    
    public int getDiasPeriodicidad(){
        return diasPeriodicidad;
    }
    
    public void setDiasPeriodicidad(int dias){
        this.diasPeriodicidad = dias;
    }
}
