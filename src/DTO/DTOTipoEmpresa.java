/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

public class DTOTipoEmpresa {
    private int codigoTipoEmpresa;
    private String nombreTipoEmpresa;

    public DTOTipoEmpresa() {
    }

    /**
     *
     * @param codigo
     * @param nombre
     */
    public DTOTipoEmpresa(int codigo, String nombre) {
        this.codigoTipoEmpresa = codigo;
        this.nombreTipoEmpresa = nombre;
    }

    public int getCodigo() {
        return codigoTipoEmpresa;
    }

    public void setCodigo(int codigo) {
        this.codigoTipoEmpresa = codigo;
    }

    public String getNombre() {
        return nombreTipoEmpresa;
    }

    public void setNombre(String nombre) {
        this.nombreTipoEmpresa = nombre;
    }
    
}

