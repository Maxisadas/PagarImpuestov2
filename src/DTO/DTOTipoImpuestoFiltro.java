/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Lorenzo
 */
public class DTOTipoImpuestoFiltro {
    private String fechaHabilitacion;
    private String fechaInhabilitacion;
    private String nombre;
    private int codigo;

    public String getNombre() {
        return nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public DTOTipoImpuestoFiltro() {
    }

    public String getFechaHabilitacion() {
        return fechaHabilitacion;
    }

    public String getFechaInhabilitacion() {
        return fechaInhabilitacion;
    }

    public void setFechaHabilitacion(String fechaHabilitacion) {
        this.fechaHabilitacion = fechaHabilitacion;
    }

    public void setFechaInhabilitacion(String fechaInhabilitacion) {
        this.fechaInhabilitacion = fechaInhabilitacion;
    }
    
}
