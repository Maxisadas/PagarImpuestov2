/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author tinch
 */
public class DTOTipoImpuestoAtributo {
    private DTOAtributo atributo;

    public DTOTipoImpuestoAtributo() {
    }

    public DTOTipoImpuestoAtributo(DTOAtributo atributo) {
        this.atributo = atributo;
    }

   

    public DTOAtributo getAtributo() {
        return atributo;
    }

    public void setAtributo(DTOAtributo atributo) {
        this.atributo = atributo;
    }
    
    
}
