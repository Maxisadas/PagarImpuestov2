/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;

/**
 *
 * @author tinch
 */
public class DTOTipoImpuestoBaja {
    private int codigo;
    private String nombre;
    private Date fechaHab;

    public DTOTipoImpuestoBaja() {
    }

    public DTOTipoImpuestoBaja(int codigo, String nombre, Date fechaHab) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.fechaHab = fechaHab;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaHab() {
        return fechaHab;
    }

    public void setFechaHab(Date fechaHab) {
        this.fechaHab = fechaHab;
    }
    
    
}
