/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Emanuel
 */
public class DTOTipoDatoAlta {
    private String nombre;
    private int codigo;
    
    public DTOTipoDatoAlta() {
    }

    public DTOTipoDatoAlta(String nombre, int codigo) {
        this.nombre = nombre;
        this.codigo = codigo;
    }
    
     /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }
    
    public void setCodigo(int codigo){
        this.codigo = codigo;
    }
}
