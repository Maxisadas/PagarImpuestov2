/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

public class DTOTipoEmpresaAlta {
    private String nombreTipoEmpresa;
    private int codigoTipoEmpresa;
    
    public DTOTipoEmpresaAlta() {
    }

    public DTOTipoEmpresaAlta(String nombre, int codigo) {
        this.nombreTipoEmpresa = nombre;
        this.codigoTipoEmpresa = codigo;
    }
    
     /**
     *
     * @return
     */
    public String getNombre() {
        return nombreTipoEmpresa;
    }

    public void setNombre(String nombre) {
        this.nombreTipoEmpresa = nombre;
    }

    public int getCodigo() {
        return codigoTipoEmpresa;
    }
    
    public void setCodigo(int codigo){
        this.codigoTipoEmpresa = codigo;
    }
}

