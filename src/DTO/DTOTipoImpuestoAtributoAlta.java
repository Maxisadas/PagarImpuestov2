package DTO;

public class DTOTipoImpuestoAtributoAlta {
    private DTOAtributoAltaTI dtoAtributoAltaTI;

    public DTOTipoImpuestoAtributoAlta() {
    }

    public DTOTipoImpuestoAtributoAlta(DTOAtributoAltaTI dtoAtributoAltaTI) {
        this.dtoAtributoAltaTI = dtoAtributoAltaTI;
    }
    

    public DTOAtributoAltaTI getDtoAtributoAltaTI() {
        return dtoAtributoAltaTI;
    }

    public void setDtoAtributoAltaTI(DTOAtributoAltaTI dtoAtributoAltaTI) {
        this.dtoAtributoAltaTI = dtoAtributoAltaTI;
    }
}