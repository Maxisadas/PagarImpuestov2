/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;

/**
 *
 * @author Cyntia
 */
public class DTOComisionEmpresaSistema {
  
    
    private String nombreTipoImpuesto;
    private Date fechaComision;
    private double porcentajeUtilizado;
    
    private int cantidadOperaciones;
    private double totalComision;
    
    private String valorPeriodo;
    private int codigoCES ;
    
    private String estado;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
    public int getCodigoCES() {
        return codigoCES;
    }

    public void setCodigoCES(int codigoCES) {
        this.codigoCES = codigoCES;
    }
    
    
    
    public String getNombreTipoImpuesto() {
        return nombreTipoImpuesto;
    }

    public void setNombreTipoImpuesto(String nombreTipoImpuesto) {
        this.nombreTipoImpuesto = nombreTipoImpuesto;
    }

    public Date getFechaComision() {
        return fechaComision;
    }

    public void setFechaComision(Date fechaComision) {
        this.fechaComision = fechaComision;
    }

    public double getPorcentajeUtilizado() {
        return porcentajeUtilizado;
    }

    public void setPorcentajeUtilizado(double porcentajeUtilizado) {
        this.porcentajeUtilizado = porcentajeUtilizado;
    }

    public int getCantidadOperaciones() {
        return cantidadOperaciones;
    }

    public void setCantidadOperaciones(int cantidadOperaciones) {
        this.cantidadOperaciones = cantidadOperaciones;
    }

    public double getTotalComision() {
        return totalComision;
    }

    public void setTotalComision(double totalComision) {
        this.totalComision = totalComision;
    }

    public String getValorPeriodo() {
        return valorPeriodo;
    }

    public void setValorPeriodo(String valorPeriodo) {
        this.valorPeriodo = valorPeriodo;
    }
    
    
    
    
    
}
