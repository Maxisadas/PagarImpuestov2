/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Mercedes
 */
public class DTOAtributoAltaTI {
    private int codigo;
    private String nombre;
    private DTOTipoDato dtoTipoDato;

    public DTOAtributoAltaTI() {
    }

    public DTOAtributoAltaTI(int codigo, String nombre, DTOTipoDato dtoTD) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.dtoTipoDato = dtoTD;
    }

    public DTOTipoDato getDtoTipoDato() {
        return dtoTipoDato;
    }

    public void setDtoTipoDato(DTOTipoDato dtoTD) {
        this.dtoTipoDato = dtoTD;
    }
    

    public DTOAtributoAltaTI(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
