/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author tinch
 */
public class DTOImpuestoConsulta {
    private int codigo;
    private String nombre;
    private boolean modificable;
    private Date fechaHab;
    private Date fechaInhab;
    private ArrayList<DTOTipoImpuestoAtributo> listaDTOTipoImpuestoAtributo = new ArrayList<>();

    public DTOImpuestoConsulta() {
    }

    public DTOImpuestoConsulta(int codigo, String nombre, boolean modificable, Date fechaHab, Date fechaInhab) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.modificable = modificable;
        this.fechaHab = fechaHab;
        this.fechaInhab = fechaInhab;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isModificable() {
        return modificable;
    }

    public void setModificable(boolean modificable) {
        this.modificable = modificable;
    }

    public Date getFechaHab() {
        return fechaHab;
    }

    public void setFechaHab(Date fechaHab) {
        this.fechaHab = fechaHab;
    }

    public Date getFechaInhab() {
        return fechaInhab;
    }

    public void setFechaInhab(Date fechaInhab) {
        this.fechaInhab = fechaInhab;
    }

    public ArrayList<DTOTipoImpuestoAtributo> getListaDTOTipoImpuestoAtributo() {
        return listaDTOTipoImpuestoAtributo;
    }

    public void setListaDTOTipoImpuestoAtributo(ArrayList<DTOTipoImpuestoAtributo> listaDTOTipoImpuestoAtributo) {
        this.listaDTOTipoImpuestoAtributo = listaDTOTipoImpuestoAtributo;
    }
    
    
}
