/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;

/**
 *
 * @author tinch
 */
public class DTOAtributoModificarAtributo {
    private String nombre;
    private int codigo;
    private DTOTipoDato dtoTD;
    private int longitud;
    private Date fechaHab;
    private Date fechaInhab;

    public DTOAtributoModificarAtributo() {
    }

    public DTOAtributoModificarAtributo(String nombre, int codigo, DTOTipoDato dtoTD, int longitud, Date fechaHab, Date fechaInhab) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.dtoTD = dtoTD;
        this.longitud = longitud;
        this.fechaHab = fechaHab;
        this.fechaInhab = fechaInhab;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public DTOTipoDato getDtoTD() {
        return dtoTD;
    }

    public void setDtoTD(DTOTipoDato dtoTD) {
        this.dtoTD = dtoTD;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public Date getFechaHab() {
        return fechaHab;
    }

    public void setFechaHab(Date fechaHab) {
        this.fechaHab = fechaHab;
    }

    public Date getFechaInhab() {
        return fechaInhab;
    }

    public void setFechaInhab(Date fechaInhab) {
        this.fechaInhab = fechaInhab;
    }
    
}
