/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author tinch
 */
public class DTOAtributoModificar {
    private int codigo;
    private String nombre;
    private int longitud;
    private DTOTipoDato dtoTD;

    public DTOAtributoModificar() {
    }

    public DTOAtributoModificar(int codigo, String nombre, int longitud, DTOTipoDato dtoTD) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.longitud = longitud;
        this.dtoTD = dtoTD;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public DTOTipoDato getDtoTD() {
        return dtoTD;
    }

    public void setDtoTD(DTOTipoDato dtoTD) {
        this.dtoTD = dtoTD;
    }
    
}
