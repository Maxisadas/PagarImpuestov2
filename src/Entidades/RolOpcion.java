/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Maxi
 */
public class RolOpcion extends Entidad {
    
    private int codigoRolOpcion;
    private Date fechaHabilitacionRolOpcion;
    private Date fechaInhabilitacionRolOpcion;
    private String nombreRolOpcion;
    public RolOpcion(){
        
        
    }
    
    public RolOpcion(String OID,int codigoRolOpcion,Date fechaHabilitacionRolOpcion, Date fechaInhabilitacionRolOpcion, String nombreRolOpcion){
        super(OID);
        this.codigoRolOpcion = codigoRolOpcion;
        this.fechaHabilitacionRolOpcion = fechaHabilitacionRolOpcion;
        this.fechaInhabilitacionRolOpcion = fechaInhabilitacionRolOpcion;
        this.nombreRolOpcion = nombreRolOpcion;
        
    }

    public int getCodigoRolOpcion() {
        return codigoRolOpcion;
    }

    public void setCodigoRolOpcion(int codigoRolOpcion) {
        this.codigoRolOpcion = codigoRolOpcion;
    }

    public Date getFechaHabilitacionRolOpcion() {
        return fechaHabilitacionRolOpcion;
    }

    public void setFechaHabilitacionRolOpcion(Date fechaHabilitacionRolOpcion) {
        this.fechaHabilitacionRolOpcion = fechaHabilitacionRolOpcion;
    }

    public Date getFechaInhabilitacionRolOpcion() {
        return fechaInhabilitacionRolOpcion;
    }

    public void setFechaInhabilitacionRolOpcion(Date fechaInhabilitacionRolOpcion) {
        this.fechaInhabilitacionRolOpcion = fechaInhabilitacionRolOpcion;
    }

    public String getNombreRolOpcion() {
        return nombreRolOpcion;
    }

    public void setNombreRolOpcion(String nombreRolOpcion) {
        this.nombreRolOpcion = nombreRolOpcion;
    }
  
    
    public String getOID() {
        return super.OID;
    }

  
    public void setOID(String OID) {
        super.OID = OID;
    }
    

    
}
