/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Cyntia
 */
public class EmpresaTipoImpuesto extends Entidad{

 private int codigoEmpresaTipoImpuesto;
 private Date fechaHabilitacionEmpresaTipoImpuesto;
 private Date fechaInhabilitacionEmpresaTipoImpuesto;

 private List<EmpresaTipoImpuestoAtributo> empresaTIA;
 
 private Empresa empresa ;

 
 private TipoImpuesto tipoImpuesto;

    public TipoImpuesto getTipoImpuesto() {
        return tipoImpuesto;
    }

    public void setTipoImpuesto(TipoImpuesto tipoImpuesto) {
        this.tipoImpuesto = tipoImpuesto;
    }
 
 
 
 
 
 
    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
 
 
 
 
 
    public int getCodigoEmpresaTipoImpuesto() {
        return codigoEmpresaTipoImpuesto;
    }

    public void setCodigoEmpresaTipoImpuesto(int codigoEmpresaTipoImpuesto) {
        this.codigoEmpresaTipoImpuesto = codigoEmpresaTipoImpuesto;
    }

   

    public Date getFechaHabilitacionEmpresaTipoImpuesto() {
        return fechaHabilitacionEmpresaTipoImpuesto;
    }

    public void setFechaHabilitacionEmpresaTipoImpuesto(Date fechaHabilitacionEmpresaTipoImpuesto) {
        this.fechaHabilitacionEmpresaTipoImpuesto = fechaHabilitacionEmpresaTipoImpuesto;
    }

    public Date getFechaInhabilitacionEmpresaTipoImpuesto() {
        return fechaInhabilitacionEmpresaTipoImpuesto;
    }

    public void setFechaInhabilitacionEmpresaTipoImpuesto(Date fechaInhabilitacionEmpresaTipoImpuesto) {
        this.fechaInhabilitacionEmpresaTipoImpuesto = fechaInhabilitacionEmpresaTipoImpuesto;
    }

 

    public EmpresaTipoImpuesto(String OID,int codigoEmpresaTipoImpuesto,List<EmpresaTipoImpuestoAtributo> listEmpresaTipoImpuestoAtributo , Date fechaHabilitacionEmpresaTipoImpuesto, Date fechaInhabilitacionEmpresaTipoImpuesto,Empresa empresa, TipoImpuesto tipoImpuesto) {
        super(OID);
        this.codigoEmpresaTipoImpuesto = codigoEmpresaTipoImpuesto;
        this.fechaHabilitacionEmpresaTipoImpuesto = fechaHabilitacionEmpresaTipoImpuesto;
        this.fechaInhabilitacionEmpresaTipoImpuesto = fechaInhabilitacionEmpresaTipoImpuesto;
        this.empresaTIA = listEmpresaTipoImpuestoAtributo;
        this.empresa = empresa;
        this.tipoImpuesto = tipoImpuesto;
    }

    public EmpresaTipoImpuesto() {
        super();
    }

    @Override
    public String getOID() {
        return super.OID;
    }    
    @Override
    public void setOID(String oidEmpresa) {
        super.OID = oidEmpresa;
    }

    public List<EmpresaTipoImpuestoAtributo> getEmpresaTIA() {
        return empresaTIA;
    }

    public void setEmpresaTIA(List<EmpresaTipoImpuestoAtributo> empresaTIA) {
        this.empresaTIA = empresaTIA;
    }
 
 
 
    
}
