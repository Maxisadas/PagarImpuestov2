package Entidades;

import java.util.Date;

/**
 * @author Fernando
 * @version 1.0
 * @created 07-Sep-2017 7:07:03 PM
 */
public class Cliente extends Entidad{

	private String apellidoCliente;
	private int dniCliente;
	private Date fechaHabilitacionCliente;
	private Date fechaInhabilitacionCliente;
	private String nombreCliente;
	private int numClienteBanco;

	public Cliente(){

	}

	public void finalize() throws Throwable {

	}
	public String getapellidoCliente(){
		return apellidoCliente;
	}

	public int getdniCliente(){
		return dniCliente;
	}

	public Date getfechaHabilitacionCliente(){
		return fechaHabilitacionCliente;
	}

	public Date getfechaInhabilitacionCliente(){
		return fechaInhabilitacionCliente;
	}

	public String getnombreCliente(){
		return nombreCliente;
	}

	public int getnumClienteBanco(){
		return numClienteBanco;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setapellidoCliente(String newVal){
		apellidoCliente = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setdniCliente(int newVal){
		dniCliente = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setfechaHabilitacionCliente(Date newVal){
		fechaHabilitacionCliente = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setfechaInhabilitacionCliente(Date newVal){
		fechaInhabilitacionCliente = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setnombreCliente(String newVal){
		nombreCliente = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setnumClienteBanco(int newVal){
		numClienteBanco = newVal;
	}
                @Override
    public String getOID() {
        return super.OID;
    }

    /**
     *
     * @param OID
     */
    @Override
    public void setOID(String OID) {
        super.OID = OID;
    }
}//end Cliente