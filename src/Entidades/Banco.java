package Entidades;

import java.util.Date;

/**
 * @author Usuario
 * @version 1.0
 * @created 07-Sep-2017 7:07:03 PM
 */
public class Banco extends Entidad{

	private String cuitBanco;
	private Date fechaHabilitacionBanco;
	private Date fechaInhabilitacionBanco;
	private String nombreBanco;

	public Banco(){

	}

	public void finalize() throws Throwable {

	}
	public String getcuitBanco(){
		return cuitBanco;
	}

	public Date getfechaHabilitacionBanco(){
		return fechaHabilitacionBanco;
	}

	public Date getfechaInhabilitacionBanco(){
		return fechaInhabilitacionBanco;
	}

	public String getnombreBanco(){
		return nombreBanco;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setcuitBanco(String newVal){
		cuitBanco = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setfechaHabilitacionBanco(Date newVal){
		fechaHabilitacionBanco = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setfechaInhabilitacionBanco(Date newVal){
		fechaInhabilitacionBanco = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setnombreBanco(String newVal){
		nombreBanco = newVal;
	}
        
           @Override
    public String getOID() {
        return super.OID;
    }

    /**
     *
     * @param OID
     */
    @Override
    public void setOID(String OID) {
        super.OID = OID;
    }
}//end Banco