/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Cyntia
 */
public class ParametroPorPeriodicidad extends Entidad{
    
    private double porcentajeAnual;
    private double porcentajeMensual;
    private double porcentajeTrimestral;
    private double porcentajeBimestral;

    public double getPorcentajeAnual() {
        return porcentajeAnual;
    }

    public void setPorcentajeAnual(double porcentajeAnual) {
        this.porcentajeAnual = porcentajeAnual;
    }

    public double getPorcentajeMensual() {
        return porcentajeMensual;
    }

    public void setPorcentajeMensual(double porcentajeMensual) {
        this.porcentajeMensual = porcentajeMensual;
    }

    public double getPorcentajeTrimestral() {
        return porcentajeTrimestral;
    }

    public void setPorcentajeTrimestral(double porcentajeTrimestral) {
        this.porcentajeTrimestral = porcentajeTrimestral;
    }

    public double getPorcentajeBimestral() {
        return porcentajeBimestral;
    }

    public void setPorcentajeBimestral(double porcentajeBimestral) {
        this.porcentajeBimestral = porcentajeBimestral;
    }
    @Override
    public String getOID() {
        return super.OID;
    }    
    @Override
    public void setOID(String oidEmpresa) {
        super.OID = oidEmpresa;
    }

    public ParametroPorPeriodicidad() {
          super();
    }
    
}
