/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Cyntia
 */
public class ComisionEmpresaSistema extends Entidad{

    
    
    
    
    private int codigoCES;
    private Date fechaComisionEmpresaSistema;
    private int periodicidadUtilizada;
    private double porcentajeUtilizado;
  
    
    private EmpresaTipoImpuesto empresaTipoImpuesto ;
   
    private List<EstadoComisionEmpresaSistema> estadoComisionEmpresaSistema;

    private List<Comision> comisiones;

    public List<Comision> getComisiones() {
        return comisiones;
    }

    public void setComisiones(List<Comision> comision) {
        this.comisiones = comision;
    }

    
    public EmpresaTipoImpuesto getEmpresaTipoImpuesto() {
        return empresaTipoImpuesto;
    }

    public void setEmpresaTipoImpuesto(EmpresaTipoImpuesto empresaTipoImpuesto) {
        this.empresaTipoImpuesto = empresaTipoImpuesto;
    }

    public List<EstadoComisionEmpresaSistema> getEstadoComisionEmpresaSistema() {
        return estadoComisionEmpresaSistema;
    }

    public void setEstadoComisionEmpresaSistema(List<EstadoComisionEmpresaSistema> estadoComisionEmpresaSistema) {
        this.estadoComisionEmpresaSistema = estadoComisionEmpresaSistema;
    }
    
    
    
    
    
    
    public int getCodigoCES() {
        return codigoCES;
    }

    public void setCodigoCES(int codigoCES) {
        this.codigoCES = codigoCES;
    }

    public Date getFechaComisionEmpresaSistema() {
        return fechaComisionEmpresaSistema;
    }

    public void setFechaComisionEmpresaSistema(Date fechaComisionEmpresaSistema) {
        this.fechaComisionEmpresaSistema = fechaComisionEmpresaSistema;
    }

    public int getPeriodicidadUtilizada() {
        return periodicidadUtilizada;
    }

    public void setPeriodicidadUtilizada(int periodicidadUtilizada) {
        this.periodicidadUtilizada = periodicidadUtilizada;
    }

    public double getPorcentajeUtilizado() {
        return porcentajeUtilizado;
    }

    public void setPorcentajeUtilizado(double porcentajeUtilizado) {
        this.porcentajeUtilizado = porcentajeUtilizado;
    }

    public ComisionEmpresaSistema() {
          super();
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @Override
    public String getOID() {
        return super.OID;
    }    
    @Override
    public void setOID(String oidEmpresa) {
        super.OID = oidEmpresa;
    }
 
    
}
