/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.Date;

/**
 *
 * @author tinch
 */
public class Usuario extends Entidad implements java.io.Serializable{
    private int codigoUsuario;
    private String nombreUsuario;
    private String contraseñaUsuario;
    private Date fechaHabilitacionUsuario;
    private Date fechaInhabilitacionUsuario;
    private Rol rol;
    private Cliente cliente;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public Date getFechaHabilitacionUsuario() {
        return fechaHabilitacionUsuario;
    }

    public void setFechaHabilitacionUsuario(Date fechaHabilitacionUsuario) {
        this.fechaHabilitacionUsuario = fechaHabilitacionUsuario;
    }

    public Date getFechaInhabilitacionUsuario() {
        return fechaInhabilitacionUsuario;
    }

    public void setFechaInhabilitacionUsuario(Date fechaInhabilitacionUsuario) {
        this.fechaInhabilitacionUsuario = fechaInhabilitacionUsuario;
    }

    public Usuario() {
        super();
    }
    
    

    public Usuario(String OID) {
        super(OID);
    }

    public Usuario(int codigoUsuario, String nombreUsuario, String contraseñaUsuario, String OID,Date fechaHabilitacionUsuario,Date fechaInhabilitacionUsuario) {
        super(OID);
        this.codigoUsuario = codigoUsuario;
        this.nombreUsuario = nombreUsuario;
        this.contraseñaUsuario = contraseñaUsuario;
        this.fechaHabilitacionUsuario = fechaHabilitacionUsuario;
        this.fechaInhabilitacionUsuario = fechaInhabilitacionUsuario;
    }
    
    

    /**
     *
     * @return
     */
    @Override
    public String getOID() {
        return super.OID;
    }

    /**
     *
     * @param OID
     */
    @Override
    public void setOID(String OID) {
        super.OID = OID;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getContraseñaUsuario() {
        return contraseñaUsuario;
    }

    public void setContraseñaUsuario(String contraseñaUsuario) {
        this.contraseñaUsuario = contraseñaUsuario;
    }
    
    
    
}
