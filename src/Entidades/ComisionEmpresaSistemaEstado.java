/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Cyntia
 */
public class ComisionEmpresaSistemaEstado extends Entidad{

    public ComisionEmpresaSistemaEstado() {
          super();
    }
    
    
    
    
    private int codigoEstadoCES;
    private String nombreEstadoCES;
   
    
    
    public int getCodigoEstadoCES() {
        return codigoEstadoCES;
    }

    public void setCodigoEstadoCES(int codigoEstadoCES) {
        this.codigoEstadoCES = codigoEstadoCES;
    }

    public String getNombreEstadoCES() {
        return nombreEstadoCES;
    }

    public void setNombreEstadoCES(String nombreEstadoCES) {
        this.nombreEstadoCES = nombreEstadoCES;
    }
    
    
    
    
    
    
    
    
    
    
      @Override
    public String getOID() {
        return super.OID;
    }    
    @Override
    public void setOID(String oidEmpresa) {
        super.OID = oidEmpresa;
    }
 
 
}
