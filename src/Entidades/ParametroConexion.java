/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.Date;

/**
 *
 * @author Maxi
 */
public class ParametroConexion extends Entidad{
    
    private int codigoConexion;
    private int codigoParametroConexion;
    private Date fechaInhabilitacionParametroConexion;
    private Date fechaHabilitacionParametroConexion;

    public int getCodigoConexion() {
        return codigoConexion;
    }

    public void setCodigoConexion(int codigoConexion) {
        this.codigoConexion = codigoConexion;
    }

    public int getCodigoParametroConexion() {
        return codigoParametroConexion;
    }

    public void setCodigoParametroConexion(int codigoParametroConexion) {
        this.codigoParametroConexion = codigoParametroConexion;
    }

    public Date getFechaInhabilitacionParametroConexion() {
        return fechaInhabilitacionParametroConexion;
    }

    public void setFechaInhabilitacionParametroConexion(Date fechaInhabilitacionParametroConexion) {
        this.fechaInhabilitacionParametroConexion = fechaInhabilitacionParametroConexion;
    }

    public Date getFechaHabilitacionParametroConexion() {
        return fechaHabilitacionParametroConexion;
    }

    public void setFechaHabilitacionParametroConexion(Date fechaHabilitacionParametroConexion) {
        this.fechaHabilitacionParametroConexion = fechaHabilitacionParametroConexion;
    }

    @Override
    public String getOID() {
        return super.OID;}

    @Override
    public void setOID(String OID) {
       super.OID = OID; }
    
    
    
}
