/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.Date;

/**
 *
 * @author Cyntia
 */
public class EstadoComisionEmpresaSistema extends Entidad{

    public EstadoComisionEmpresaSistema() {
          super();
    }
    
    
    
    private Date fechaEstadoCES;
    private ComisionEmpresaSistemaEstado comisionEmpresaSistemaEstado;

    public ComisionEmpresaSistemaEstado getComisionEmpresaSistemaEstado() {
        return comisionEmpresaSistemaEstado;
    }

    public void setComisionEmpresaSistemaEstado(ComisionEmpresaSistemaEstado comisionEmpresaSistemaEstado) {
        this.comisionEmpresaSistemaEstado = comisionEmpresaSistemaEstado;
    }
    
    
    
    public Date getFechaEstadoCES() {
        return fechaEstadoCES;
    }

    public void setFechaEstadoCES(Date fechaEstadoCES) {
        this.fechaEstadoCES = fechaEstadoCES;
    }
    
    
    
    
    
    
      @Override
    public String getOID() {
        return super.OID;
    }    
    @Override
    public void setOID(String oidEmpresa) {
        super.OID = oidEmpresa;
    }
 
}
