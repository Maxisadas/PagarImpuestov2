/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Maxi
 */
public class OperacionAtributo extends Entidad {
    
    private int valorOperacionAtibuto;
    private TipoImpuesto tipoImpuesto;

    public int getValorOperacionAtibuto() {
        return valorOperacionAtibuto;
    }

    public void setValorOperacionAtibuto(int valorOperacionAtibuto) {
        this.valorOperacionAtibuto = valorOperacionAtibuto;
    }

    public TipoImpuesto getTipoImpuesto() {
        return tipoImpuesto;
    }

    public void setTipoImpuesto(TipoImpuesto tipoImpuesto) {
        this.tipoImpuesto = tipoImpuesto;
    }

    @Override
    public String getOID() {
        return super.OID;
   }

    @Override
    public void setOID(String OID) {
        super.OID = OID;
 }
    
}
