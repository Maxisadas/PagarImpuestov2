package Entidades;

import java.util.Date;

/**
 * @author Fernando
 * @version 1.0
 * @created 07-Sep-2017 7:07:07 PM
 */
public class Cuenta extends Entidad{

	private Date fechaHabilitacionCuenta;
	private Date fechaInhabilitacionCuenta;
	private int numeroCuenta;
        private Cliente cliente;
        private Banco cuentaBanco;
        private TipoCuenta tipoCuenta;
        
    public Cuenta(String OID,Date fechaHabilitacionCuenta,Date fechaInhabilitacionCuenta,int numeroCuenta,Cliente cliente,Banco cuentaBanco,TipoCuenta tipocuenta){
        
        this.OID = OID;
        this.cliente = cliente;
        this.cuentaBanco = cuentaBanco;
        this.fechaHabilitacionCuenta = fechaHabilitacionCuenta;
        this.fechaInhabilitacionCuenta = fechaInhabilitacionCuenta;
        this.numeroCuenta = numeroCuenta;
        this.tipoCuenta = tipoCuenta;
        
    }

    public Date getFechaHabilitacionCuenta() {
        return fechaHabilitacionCuenta;
    }

    public void setFechaHabilitacionCuenta(Date fechaHabilitacionCuenta) {
        this.fechaHabilitacionCuenta = fechaHabilitacionCuenta;
    }

    public Date getFechaInhabilitacionCuenta() {
        return fechaInhabilitacionCuenta;
    }

    public void setFechaInhabilitacionCuenta(Date fechaInhabilitacionCuenta) {
        this.fechaInhabilitacionCuenta = fechaInhabilitacionCuenta;
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Banco getCuentaBanco() {
        return cuentaBanco;
    }

    public void setCuentaBanco(Banco cuentaBanco) {
        this.cuentaBanco = cuentaBanco;
    }

    public TipoCuenta getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(TipoCuenta tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    
                @Override
    public String getOID() {
        return super.OID;
    }

    /**
     *
     * @param OID
     */
    @Override
    public void setOID(String OID) {
        super.OID = OID;
    }
}//end Cuenta