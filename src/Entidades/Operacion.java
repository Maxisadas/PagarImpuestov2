/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Cyntia
 */
public class Operacion  extends Entidad{
    
    private String codPagoElect;
    private double importeOperacion;
    private Date fechaOperacion;
    private Date fechaVencimientoComprobanteImpago;
    private static int numeroOperacion;
    private int numeroComprobante;
    private TipoImpuesto tipoImpuesto ;
    private EmpresaTipoImpuesto empresaTipoImpuesto;
    private Cuenta cuenta;
    private Cliente cliente;
    private List<OperacionAtributo> listOperacionAtributo;

    public EmpresaTipoImpuesto getEmpresaTipoImpuesto() {
        return empresaTipoImpuesto;
    }

    public void setEmpresaTipoImpuesto(EmpresaTipoImpuesto empresaTipoImpuesto) {
        this.empresaTipoImpuesto = empresaTipoImpuesto;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<OperacionAtributo> getListOperacionAtributo() {
        return listOperacionAtributo;
    }

    public void setListOperacionAtributo(List<OperacionAtributo> listOperacionAtributo) {
        this.listOperacionAtributo = listOperacionAtributo;
    }

    public String getcodPagoElect() {
        return codPagoElect;
    }

    public void setCodPagoElect(String codPagoElect) {
        this.codPagoElect = codPagoElect;
    }


    public double getImporteOperacion() {
        return importeOperacion;
    }

    public void setImporteOperacion(double importeOperacion) {
        this.importeOperacion = importeOperacion;
    }

    public Date getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(Date fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public Date getFechaVencimientoComprobanteImpago() {
        return fechaVencimientoComprobanteImpago;
    }

    public void setFechaVencimientoComprobanteImpago(Date fechaVencimientoComprobanteImpago) {
        this.fechaVencimientoComprobanteImpago = fechaVencimientoComprobanteImpago;
    }

   

    public int getNumeroOperacion() {
        return numeroOperacion;
    }

    public void setNumeroOperacion(int numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    public int getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(int numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }
    
      @Override
    public String getOID() {
        return super.OID;
    }    
    @Override
    public void setOID(String oidEmpresa) {
        super.OID = oidEmpresa;
    }

    public TipoImpuesto getTipoImpuesto() {
        return tipoImpuesto;
    }

    public void setTipoImpuesto(TipoImpuesto tipoImpuesto) {
        this.tipoImpuesto = tipoImpuesto;
    }

    public Operacion() {
          super();
    }
    
    
}
