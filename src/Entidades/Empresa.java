/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;
import java.util.Date;

public class Empresa extends Entidad {
    private String cuitEmpresa;
    private String nombreEmpresa;
    private Date fechaHabilitacionEmpresa;
    private Date fechaInhabilitacionEmpresa;
    private TipoEmpresa tipoEmpresa;
 
    
    public Empresa(){
        super();
    }    
    public Empresa(String oid){
        super(oid);
    }
    public Empresa(String OID, String cuit, String nombreEmpresa, Date fechaHab, Date fechaInhab, TipoEmpresa tipo){
        super(OID);
        this.cuitEmpresa = cuit;
        this.nombreEmpresa = nombreEmpresa;
        this.fechaHabilitacionEmpresa = fechaHab;
        this.fechaInhabilitacionEmpresa = fechaInhab;
        this.tipoEmpresa = tipo;
      
    }
     @Override
    public String getOID() {
        return super.OID;
    }    
    @Override
    public void setOID(String oidEmpresa) {
        super.OID = oidEmpresa;
    }
    
    public String getCuitEmpresa(){
        return cuitEmpresa;
    }
    public void setCuitEmpresa(String cuit){
        this.cuitEmpresa = cuit;
    }
    public String getNombreEmpresa(){
        return nombreEmpresa;
    }
    public void setNombreEmpresa(String nombre){
        this.nombreEmpresa = nombre;
    }
    public Date getFechaHabEmpresa(){
        return fechaHabilitacionEmpresa;
    }
    public void setFechaHabEmpresa(Date fechaHabEmpresa){
        this.fechaHabilitacionEmpresa = fechaHabEmpresa;
    }
    public Date getFechaInhabEmpresa(){
        return fechaInhabilitacionEmpresa;
    }
    public void setFechaInhabEmpresa(Date fechaInhabEmpresa){
        this.fechaInhabilitacionEmpresa = fechaInhabEmpresa;
    }
    public TipoEmpresa getTipoEmpresa(){
        return tipoEmpresa;
    }
    public void  setTipoEmpresa(TipoEmpresa tipo){
        this.tipoEmpresa = tipo;
    }


    public Date getFechaHabilitacionEmpresa() {
        return fechaHabilitacionEmpresa;
    }

    public void setFechaHabilitacionEmpresa(Date fechaHabilitacionEmpresa) {
        this.fechaHabilitacionEmpresa = fechaHabilitacionEmpresa;
    }

    public Date getFechaInhabilitacionEmpresa() {
        return fechaInhabilitacionEmpresa;
    }

    public void setFechaInhabilitacionEmpresa(Date fechaInhabilitacionEmpresa) {
        this.fechaInhabilitacionEmpresa = fechaInhabilitacionEmpresa;
    }


    
    
}
