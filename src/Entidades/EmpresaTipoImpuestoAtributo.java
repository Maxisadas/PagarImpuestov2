/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Cyntia
 */
public class EmpresaTipoImpuestoAtributo extends Entidad {

    private String Orden;
    private String formato;
    private String valorPeriodicidad;
    private TipoImpuestoAtributo tipoimpuestoatributo;

    public TipoImpuestoAtributo getTipoimpuestoatributo() {
        return tipoimpuestoatributo;
    }

    public void setTipoimpuestoatributo(TipoImpuestoAtributo tipoimpuestoatributo) {
        this.tipoimpuestoatributo = tipoimpuestoatributo;
    }


    public void setOrden(String Orden) {
        this.Orden = Orden;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getValorPeriodicidad() {
        return valorPeriodicidad;
    }

    public void setValorPeriodicidad(String valorPeriodicidad) {
        this.valorPeriodicidad = valorPeriodicidad;
    }

    public EmpresaTipoImpuestoAtributo(String Orden, String formato, String valorPeriodicidad) {
        this.Orden = Orden;
        this.formato = formato;
        this.valorPeriodicidad = valorPeriodicidad;
    }

    public EmpresaTipoImpuestoAtributo() {
    }

    @Override
    public String getOID() {
        return super.OID;
    }

    @Override
    public void setOID(String oidEmpresa) {
        super.OID = oidEmpresa;
    }

    public String getOrden() {
        return Orden;
    }

}
