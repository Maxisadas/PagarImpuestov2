/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.Date;
import java.util.List;

/**
 *
 * @author phantom2024
 */
public class Rol extends Entidad {
    
    private int codigoRol;
    private Date fechaHabilitacionRol;
    private Date fechaInhabilitacionRol;
    private String nombreRol;
    private List<RolOpcion> ListRolOpcion;
    
    
    public Rol(){
        
        
    }
    
    public Rol(String OID,int codigoRol,Date fechaHabilitacionRol,Date fechaInhabilitacionRol,String nombreRol){
        super(OID);
        this.codigoRol = codigoRol;
        this.fechaHabilitacionRol = fechaHabilitacionRol;
        this.fechaInhabilitacionRol = fechaInhabilitacionRol;
        this.nombreRol = nombreRol;
        
        
    }

    public int getCodigoRol() {
        return codigoRol;
    }

    public void setCodigoRol(int codigoRol) {
        this.codigoRol = codigoRol;
    }

    public Date getFechaHabilitacionRol() {
        return fechaHabilitacionRol;
    }

    public void setFechaHabilitacionRol(Date fechaHabilitacionRol) {
        this.fechaHabilitacionRol = fechaHabilitacionRol;
    }

    public Date getFechaInhabilitacionRol() {
        return fechaInhabilitacionRol;
    }

    public void setFechaInhabilitacionRol(Date fechaInhabilitacionRol) {
        this.fechaInhabilitacionRol = fechaInhabilitacionRol;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }
    

    public List<RolOpcion> getListRolOpcion() {
        return ListRolOpcion;
    }

    public void setListRolOpcion(List<RolOpcion> ListRolOpcion) {
        this.ListRolOpcion = ListRolOpcion;
    }

 

   public String getOID() {
        return super.OID;
    }

  
    public void setOID(String OID) {
        super.OID = OID;
    }
    
    
}
