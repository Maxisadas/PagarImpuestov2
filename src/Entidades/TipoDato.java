/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.Date;

/**
 *
 * @author tinch
 */
public class TipoDato extends Entidad{
    private int codTipoDato;
    private String nombreTipoDato;
    private Date fechaHabilitacionTipoDato;
    private Date fechaInhabilitacionTipoDato;
    private int longitudTipoDato;

    public int getLongitudTipoDato() {
        return longitudTipoDato;
    }

    public void setLongitudTipoDato(int longitudTipoDato) {
        this.longitudTipoDato = longitudTipoDato;
    }

    public TipoDato() {
        super();
    }

    public TipoDato(String OID) {
        super(OID);
    }

    public TipoDato(int codTipoDato, String nombreTipoDato, Date fechaHabTipoDato, Date fechaInhabTipoDato, String OID,int longitudTipoDato) {
        super(OID);
        this.codTipoDato = codTipoDato;
        this.nombreTipoDato = nombreTipoDato;
        this.fechaHabilitacionTipoDato = fechaHabTipoDato;
        this.fechaInhabilitacionTipoDato = fechaInhabTipoDato;
        this.longitudTipoDato = longitudTipoDato;
    }

    @Override
    public String getOID() {
        return super.OID;
    }

    @Override
    public void setOID(String OID) {
        super.OID = OID;
    }

    public int getCodTipoDato() {
        return codTipoDato;
    }

    public void setCodTipoDato(int codTipoDato) {
        this.codTipoDato = codTipoDato;
    }

    public String getNombreTipoDato() {
        return nombreTipoDato;
    }

    public void setNombreTipoDato(String nombreTipoDato) {
        this.nombreTipoDato = nombreTipoDato;
    }

    public Date getFechaHabilitacionTipoDato() {
        return fechaHabilitacionTipoDato;
    }

    public void setFechaHabilitacionTipoDato(Date fechaHabTipoDato) {
        this.fechaHabilitacionTipoDato = fechaHabTipoDato;
    }

    public Date getFechaInhabilitacionTipoDato() {
        return fechaInhabilitacionTipoDato;
    }

    public void setFechaInhabilitacionTipoDato(Date fechaInhabilitacionTipoDato) {
        this.fechaInhabilitacionTipoDato = fechaInhabilitacionTipoDato;
    }
    
    
    
    
    
    
}
